<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Report</title>


    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>

    <script type="text/javascript" src="/js/jquery.datetimepicker.min.js"></script>

    <link rel="stylesheet" href="/css/app.css" />
    <link rel="stylesheet" href="/css/jquery.datetimepicker.min.css" />

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            margin: 0;
        }

        .row {
            min-height: 100vh;
        }

        .row > div {
            padding: 20px;
        }

        .row > div:last-child {
            border-left: 1px solid #eeeeee;
        }
    </style>

    <script type="text/javascript">
        $.datetimepicker.setDateFormatter('moment');

        $(document).ready(function() {
            $('#datetime_from').datetimepicker();
            $('#datetime_to').datetimepicker();
        });
    </script>
</head>
<body>

<div class="container">

    <div class="row">

        <div class="col-sm-12 col-md-4 col-lg-3">
            <form>
                <div class="form-group">
                    <label for="client_id">Name <span class="red">*</span></label>

                    <select id="client_id" name="client_id" class="form-control">
                        <option value="">&laquo;Select user&raquo;</option>
                        @foreach ($clients as $user)
                            <option value="{{ $user->id }}" @if (app('request')->input('client_id') == $user->id) selected @endif>{{ $user->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="datetime_from">From</label>
                    <input class="form-control" id="datetime_from" name="datetime_from" autocomplete="off" value="{{ app('request')->input('datetime_from') }}">
                </div>

                <div class="form-group">
                    <label for="datetime_to">To</label>
                    <input class="form-control" id="datetime_to" name="datetime_to" autocomplete="off" value="{{ app('request')->input('datetime_to') }}">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary mb-2">Show report</button>
                </div>
            </form>
        </div>

        <div class="col">
            @if ($errors->any())
                <div class="text-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if ($client)
                <h5>
                    Total:
                        Income {{ $summary->income }} {{ strtoupper($client->currency) }}
                        @if ($client->currency != 'usd') ({{ round($summary->income * $client->walletCurrency->rate, 2) }} USD)@endif
                        &ndash; Outcome {{ $summary->outcome }} {{ strtoupper($client->currency) }}
                        @if ($client->currency != 'usd') ({{ round($summary->outcome * $client->walletCurrency->rate, 2) }} USD)@endif

                        <a href="{{ action('ReportController@export', [
                            'client_id' => app('request')->input('client_id'),
                            'datetime_from' => app('request')->input('datetime_from'),
                            'datetime_to' => app('request')->input('datetime_to'),
                        ]) }}" class="btn btn-link">Download CSV</a>
                </h5>
            @endif

            <table class="table w-100">
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Date/Time</th>
                    <th scope="col">Amount @if ($client)({{ strtoupper($client->currency) }}) @endif</th>
                    <th scope="col">Comment</th>
                </tr>
                @if ($transactions)
                    @foreach ($transactions as $transaction)
                        <tr>
                            <td>{{ $transaction->id }}</td>
                            <td>{{ $transaction->created_at }}</td>
                            <td>{{ $transaction->amount }}</td>
                            <td>
                                @if ($transaction->partner_id)
                                    @if ($transaction->amount > 0) Received from {{ $transaction->partner['name'] }} @endif
                                    @if ($transaction->amount < 0) Sent to {{ $transaction->partner['name'] }} @endif
                                @else
                                    Deposited to account directly
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="4">There are no items</td>
                    </tr>
                @endif
            </table>

            @if ($transactions)
                {{ $transactions->appends([
                       'client_id' => app('request')->input('client_id'),
                       'datetime_from' => app('request')->input('datetime_from'),
                       'datetime_to' => app('request')->input('datetime_to'),
                   ])
                   ->links()
                }}
            @endif
        </div>

    </div>

</div>

</body>
</html>
