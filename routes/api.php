<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/create', 'ApiController@create');

Route::post('/currency', 'ApiController@currency');

Route::post('/deposit', 'ApiController@deposit');

Route::post('/transfer', 'ApiController@transfer');
