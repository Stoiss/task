<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'country',
        'city',
        'currency'
    ];

    /**
     * @var array
     */
    protected $attributes = [
        'balance' => 0,
    ];

    /**
     * Many to One relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function walletCurrency()
    {
        return $this->belongsTo('App\Currency', 'currency');
    }
}
