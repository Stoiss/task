<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Controller;
use App\Currency;
use App\Client;
use App\Transaction;

class ApiController extends Controller
{
    /**
     * Creating a new client
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'country' => 'required|max:255',
            'city' => 'required|max:255',
            'currency' => 'required|exists:currencies,code'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => '0',
                'message' => $validator->errors()->first()
            ]);
        }

        $client = new Client;
        $client->name = $request->input('name');
        $client->country = $request->input('country');
        $client->city = $request->input('city');
        $client->currency = $request->input('currency');
        $client->save();

        return response()->json([
            'success'   => '1',
            'client_id' => $client->id
        ]);
    }

    /**
     * Deposit money to an account
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deposit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'client_id' => 'required|exists:clients,id',
            'amount' => 'required|regex:/^\\d+(\\.\\d{1,2})?$/',
            'currency' => 'nullable|exists:currencies,code'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => '0',
                'message' => $validator->errors()->first()
            ]);
        }

        $amount = (float) $request->input('amount');

        $client = Client::find($request->input('client_id'));
        $currencyClient = Currency::find($client->currency);

        if ($request->input('currency')) {
            $currencyIncome = Currency::find($request->input('currency'));
            $amount = round($amount * $currencyIncome->rate / $currencyClient->rate, 2);
        }

        DB::beginTransaction();

        $transaction = new Transaction;
        $transaction->client_id = $client->id;
        $transaction->amount = $amount;
        $transaction->usd = $amount * $currencyClient->rate;
        $transaction->save();

        $client->balance += $amount;
        $client->save();

        DB::commit();

        return response()->json([
            'success' => '1',
            'transaction_id' => $transaction->id
        ]);
    }

    /**
     * Transfer money from some account to another
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function transfer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sender_id' => 'required|exists:clients,id',
            'recipient_id' => 'required|exists:clients,id',
            'amount' => 'required|regex:/^\\d+(\\.\\d{1,2})?$/'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => '0',
                'message' => $validator->errors()->first()
            ]);
        }

        if ($request->input('sender_id') == $request->input('recipient_id')) {
            return response()->json([
                'success' => '0',
                'message' => 'The accounts are the same.'
            ]);
        }

        $client = Client::find($request->input('sender_id'));
        $currencyClient = Currency::find($client->currency);

        $amountClient = (float) $request->input('amount');

        if ($amountClient > $client->balance) {
            return response()->json([
                'success' => '0',
                'message' => 'The client balance is not enough for this transaction.'
            ]);
        }

        $partner = Client::find($request->input('recipient_id'));
        $currencyPartner = Currency::find($partner->currency);

        $amountUsd = $amountClient * $currencyClient->rate;
        $amountPartner = round($amountUsd / $currencyPartner->rate, 2);

        DB::beginTransaction();

        $transactionClient = new Transaction;
        $transactionClient->client_id = $client->id;
        $transactionClient->partner_id = $partner->id;
        $transactionClient->amount = -$amountClient;
        $transactionClient->usd = -$amountUsd;
        $transactionClient->save();

        $transactionPartner = new Transaction;
        $transactionPartner->client_id = $partner->id;
        $transactionPartner->partner_id = $client->id;
        $transactionPartner->amount = $amountPartner;
        $transactionPartner->usd = $amountUsd;
        $transactionPartner->save();

        $client->balance -= $amountClient;
        $client->save();

        $partner->balance += $amountPartner;
        $partner->save();

        DB::commit();

        return response()->json([
            'success' => '1',
            'client_transaction_id' => $transactionClient->id,
            'partner_transaction_id' => $transactionPartner->id
        ]);
    }

    /**
     * Creating/Updating currencies
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function currency(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required|min:3|max:3',
            'rate' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => '0',
                'message' => $validator->errors()->first()
            ]);
        }

        Currency::updateOrCreate(
            [
                'code' => strtolower($request->input('code'))
            ],
            [
                'rate' => $request->input('rate')
            ]
        );

        return response()->json([
            'success' => '1'
        ]);
    }
}
