<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use App\Currency;
use App\Client;
use App\Transaction;

class ReportController extends Controller
{
    /**
     * The main report page
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $transactions = [];
        $client = null;
        $summary = null;

        if ($request->input('client_id')) {
            $request->validate([
                'client_id' => 'required|exists:clients,id',
                'datetime_from' => 'nullable|date',
                'datetime_to' => 'nullable|date',
            ]);

            $client = Client::find($request->input('client_id'));

            $condition = [
                ['client_id', '=', $request->input('client_id')]
            ];

            if ($request->input('datetime_from')) {
                $condition[] = ['created_at', '>=', $request->input('datetime_from')];
            }

            if ($request->input('datetime_to')) {
                $condition[] = ['created_at', '<=', $request->input('datetime_to')];
            }

            $transactions = Transaction::where($condition)
                ->orderBy('id', 'asc')
                ->paginate(50);

            $selectRaw = [
                'sum(case when amount > 0 then amount else 0 end) as income',
                'sum(case when amount < 0 then abs(amount) else 0 end) as outcome'
            ];

            $summary = DB::table('transactions')
                ->select(DB::raw(implode(', ', $selectRaw)))
                ->where($condition)
                ->first();
        }

        return view('report', [
            'clients' => Client::all()->sortBy('name'),
            'transactions' => $transactions,
            'client' => $client,
            'summary' => $summary
        ]);
    }

    /**
     * Export results to csv
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function export(Request $request)
    {
        $request->validate([
            'client_id' => 'required|exists:clients,id',
            'datetime_from' => 'nullable|date',
            'datetime_to' => 'nullable|date',
        ]);

        $client = Client::find($request->input('client_id'));

        $condition = [
            ['client_id', '=', $request->input('client_id')]
        ];

        if ($request->input('datetime_from')) {
            $condition[] = ['created_at', '>=', $request->input('datetime_from')];
        }

        if ($request->input('datetime_to')) {
            $condition[] = ['created_at', '<=', $request->input('datetime_to')];
        }

        $callback = function() use ($client, $condition)
        {
            $file = fopen('php://output', 'w');

            fputcsv($file, ['ID', 'Date/Time', 'Amount (' . strtoupper($client->currency) . ')', 'Comment']);

            $transactions = Transaction::where($condition)
                ->orderBy('id', 'asc')
                ->get();

            foreach ($transactions as $transaction) {
                if ($transaction->partner_id) {
                    $comment = $transaction->amount > 0
                        ? 'Received from ' . $transaction->partner->name
                        : 'Sent to ' . $transaction->partner->name;
                } else {
                    $comment = 'Deposited to account directly';
                }

                fputcsv($file, [
                    $transaction->id,
                    $transaction->created_at,
                    $transaction->amount,
                    $comment
                ]);
            }

            fclose($file);
        };

        return response()->stream($callback, 200, [
            'Content-type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename=client_' . $client->id . '.csv',
            'Pragma' => 'no-cache',
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Expires' => '0'
        ]);
    }
}
