<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    /**
     * @var string
     */
    protected $primaryKey = 'code';

    /**
     * @var array
     */
    protected $fillable = ['code', 'rate'];
}
